#!/usr/bin/env python3

import socket
import sys

'''
Microphone module for gun detection

Uses socket to read data out of LOUROE Microphone. Provides a class MicrophoneSocket()
to simplify socket.socket in the context of microphone data processing. Also provides
main() function to implement continuous data buffering.

Meant to be imported and called in gun_detection.py

Can be tested directly from command line

Call with `python3 get_mic_input.py {IPv4}:{Port}`
(IPv4/Port correspond to address on which to recieve data)

'''


class MicrophoneSocket:

    '''Class used to represent the microphone socket

    Attributes
    ----------
    address : string
        IP address which the microphone binds to
    port : string
        Port from which to read microphone outputs
    socket : socket.socket
        Socket which recieves the microphone output

    Methods
    -------
    bind()
        Binds the socket to its address and port
    accept()
        Returns a connection socket to the bound address/port
    read(conn=self.accept())
        Reads data out of a connection socket
    get_address()
        Returns IP/Port as a formatted string
    close_conn(conn)
        Closes a connection socket
    close()
        Closes microphone socket
    '''

    def __init__(self, address, port):
        '''
        Parameters
        ----------
        address : string
            IPv4 address of the socket
        port : uint
            Port number of the socket
        '''
        self.address = address
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def bind(self):
        '''Binds socket to address and port'''
        self.socket.bind((self.address, self.port))
        self.socket.listen(5)

    def accept(self):
        '''Creates connection socket

        * Requires microphone socket to be bound

        Returns
        -------
        conn : socket.socket
            Connection socket bound to self.address at self.port
        '''
        (conn, add) = self.socket.accept()
        return conn

    def read(self, conn=None):
        if conn == None:
            conn = self.accept()
        '''Reads data out of connection socket

        Parameters
        ----------
        conn : socket.socket
            Connection socket (default creates a connection socket)

        Returns
        -------
        data : bstr
            Read data from connection socket
        '''
        data = conn.recv(64)
        return data

    def get_address(self):
        '''Formats IP address and port number into a string

        Returns
        -------
        address : str
            String formatted as {self.address}:{self.port}
        '''
        address = self.address + ':' + str(self.port)
        return address

    def close_conn(self, conn):
        '''Closes connection socket

        Parameters
        ----------
        conn : socket.socket
            Connection socket to close
        '''
        conn.close()

    def close(self):
        '''Closes microphone socket'''
        self.socket.close()


def main(address, port):

    '''Function to search for a gunshot in the microphone feed

    Parameters
    ----------
    address : string
        IPv4 address to recieve microphone data
    port : uint
        Port on which to recieve microphone data

    Returns
    -------
    code : bool
        True if b'Gunshot' in microphone feed
    '''

    mic_sock = MicrophoneSocket(address, port)
    mic_sock.bind()
    print("Bound socket to {}. Accepting connection...".format(mic_sock.get_address()))

    conn = mic_sock.accept()

    print("Accepted connection. Reading...")

    while True:

        string = mic_sock.read(conn)
        print(string)
        if b'Gunshot' in string:
            mic_sock.close_conn(conn)
            mic_sock.close()
            return True

if __name__ == "__main__":

    print("Running debug...")

    string = sys.argv[1]
    arr = string.split(':')

    if main(arr[0], int(arr[1])):
        print("Gunshot detected!")

    print("Exiting...")
