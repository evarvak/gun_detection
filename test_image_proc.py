import os
import cv2
from PIL import Image
import numpy as np
import time

count = 0
loc = 'test'
arr = os.listdir('./test')
arr = [loc + '/' + img for img in arr]

for image in arr:

    ts = time.time()

    # image = Image.open(image)
    image_np = cv2.imread(image)
    ts1 = time.time()
    d1 = ts1 - ts
    # image_np = load_image_into_numpy_array(image)
    image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    ts2 = time.time()
    d2 = ts2 - ts1

    image_np_exp = np.expand_dims(image_np, axis=0)
    ts3 = time.time()
    d3 = ts3 - ts2

    count += 1

    delta = ts3 - ts

    print("FPS: {}, Time to open: {}, NP Array: {}, Expand Dimensions: {}".format(1/delta, d1, d2, d3))

