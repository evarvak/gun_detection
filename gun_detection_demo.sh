#!/bin/bash

## This bash script runs the gun detection demo. Before running the script, please ensure
## that you have the following directory structure:
##
## .
## | detect_guns.py
## | get_mic_input.py
## | email_image.py
## | converter.py
## | object-detection.pbtxt
## | gun_detection_demo.sh
## | frozen_inference_graph.pb
## | utils/
##     | label_map_util.py
##     | visualization_utils.py
## | (Optional) trt_graph.pb
##
## NOTE: Having additional files in your working directory will not affect preformance.

# Creates TensorRT graph if none found

if [ $1 ]; then
    echo "[Argument]    [Description]                               [Default value]"
    echo "MODEL         Name of the frozen graph file               (default is ./trt_graph.pb)"
    echo "LABELS        Name of the labels file                     (default is ./object-detection.pb)"
    echo "MICROPHONE    IPv4 and port of Louroe Microphone output   (default is 127.0.0.1:5690)"
    echo "EMAIL         Gmail address of the sender                 (default is CIMCONObjectDetection@gmail.com)"
    echo "PASSWORD      Password of the sender                      (default is Cimcon100)"
    echo "RECIPIENTS    List of recipients seperated with comma     (default is CIMCONObjectDetection@gmail.com)"
    echo "VIDEO         Location of RTSP or h264/h265 video         (default is ./test.mp4)"
    echo ""
    echo "Correct usage example: "
    echo "MICROPHONE=\"192.168.6.55:5690\"" RECIPIENTS=\"Edward.Varvak@cimconlighting.com, evarvak@gmail.com\" VIDEO=\"rtsp://192.168.6.6:554/live.sdp\" ./gun_detection_demo.sh
    exit 1
fi


if [ ! $MODEL ]; then
    MODEL="./trt_graph.pb"
fi
echo "Using model: $MODEL"

if [ ! $LABELS ]; then
    LABELS="./object-detection.pbtxt"
fi
echo "Using labels: $LABELS"

if [ ! "$MICROPHONE" ]; then
    MICROPHONE="127.0.0.1:5690"
fi
echo "Reading microphone from $MICROPHONE"

if [ ! "$EMAIL" ]; then
    EMAIL="CIMCONObjectDetector@gmail.com"
    PASSWORD="Cimcon100"
fi
echo "Using email $EMAIL"

if [ ! "$RECIPIENTS" ]; then
    RECIPIENTS=$EMAIL
fi
echo "Recipients list: [$RECIPIENTS]"

if [ ! "$VIDEO" ]; then
    VIDEO="./test.mp4"
fi
echo "Processing on video $VIDEO"

if [ ! -e trt_graph.pb ]; then
    echo "Converting to TensorRT..."
    python3 converter.py ./frozen_inference_graph.sh
    echo "Made TensorRT graph"
fi

echo "Running gun detection..."
python3 detect_guns.py \
    --model "$MODEL" \
    --labels "$LABELS" \
    --microphone "$MICROPHONE" \
    --email "$EMAIL" \
    --password "$PASSWORD" \
    --recipients "$RECIPIENTS" \
    --video "$VIDEO"

echo "Finished running gun detection"

