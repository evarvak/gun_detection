import cv2
# from PIL import Image
import numpy as np
import time

url = '/home/jetson/test.mp4'
count = 0
vidcap = cv2.VideoCapture(url)

w = int(vidcap.get(3))
h = int(vidcap.get(4))

out = cv2.VideoWriter('out.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 25, (w, h))


read_times, proc_times, write_times, fps = [], [], [], []

while vidcap.isOpened():

    ts = time.time()

    # cv2.destroyAllWindows()

    ret, img = vidcap.read()

    ts1 = time.time()

    if img is None or ret is False:
        break

    image_np = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    image_np_exp = np.expand_dims(image_np, axis=0)
    img = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)

    ts2 = time.time()

    # cv2.imshow('frame{}'.format(count), img)
    out.write(img)
    count += 1

    ts3 = time.time()

    k = cv2.waitKey(10)
    if k == 27:
        break

    read_times.append(ts1 - ts)
    proc_times.append(ts2 - ts1)
    write_times.append(ts3 - ts2)
    fps.append(1 / (ts3 - ts))
    print("Read Time: {}, Processing Time: {}, Write Time: {}, FPS: {}".format(read_times[-1], proc_times[-1], write_times[-1], fps[-1]))

vidcap.release()
out.release()

def avg(l):
    return sum(l) / len(l)

print("\nAverage Read Time: {}\nAverage Processing Time: {}\nAverage Write Time: {}\nAverage FPS: {}".format(avg(read_times), avg(proc_times), avg(write_times), avg(fps)))
# cv2.destroyAllWindows()
