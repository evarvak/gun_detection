#!/usr/bin/env python3
# coding: utf-8

"""Gun detection demo script

Runs a demonstration of gun detection for NS360 on the Nvidia Jetson Nano

Dependencies:
    * Tensorflow-GPU
    * Python 3.6+
    * Numpy
    * OpenCV 3.4+
    * CUDA / TensorRT

Pipeline for demo:
    [1] Reads tensorflow graph into shared memory
    [2] Reads labels into memory
    [3] Locates output layers in tensorflow graph
    [4] Opens socket to recieve microphone data
    [5] Waits for microphone to detect gunshot
    [6] Once gunshot is detected, processes video feed
    [7] If a gun is found in the feed, the frame is sent to recipients via email

Arguments:
    --model         Path to tensorflow frozen graph (trt_graph.pb)
    --labels        Path to labels file (object-detection.pbtxt)
    --microphone    Address/Port on which to listen for microphone data
    --email         Valid gmail address
    --password      Password corresponding to gmail address
    --recipients    List of recipients (default is author)
    --video         Location of video feed (RTSP or h264/h265)

Example: ::

    python3 detect_guns.py \
        --model "./outputs/ssd_mobilenet/ssd_mobile/ssd_mobile_graph_output/trt_graph.pb"\
        --labels "./output/ssd_mobilenet/ssd_mobile/ssd_mobile_graph_output/object-detection.pbtxt" \
        --microphone "192.168.6.15:5600"
        --email "evarvak@gmail.com" \
        --password "Password123" \
        --recipients "evarvak@gmail.com, Edward.Varvak@cimconlighting.com" \
        --video "/home/jetson/test.mp4"


If trt_graph.pb does not exist, download a frozen graph (frozen_inference_graph.pb)
and run `python3 converter.py path/to/frozen_inference_graph.pb` using converter.py

"""

import time
import numpy as np
import os
import sys
import tensorflow as tf

import tensorflow.contrib.tensorrt as trt

import cv2

# Local imports -- Make sure that these files exist
# in their respective directories
from utils import label_map_util
from utils import visualization_utils as vis_util
import get_mic_input as mic
import email_image as email


# Parameter definitions (to run from shell)
import argparse

ps = argparse.ArgumentParser(description='Detects gun based on video and audio input',
        formatter_class=argparse.RawTextHelpFormatter)

ps.add_argument("--model", type=str, help="Path to TensorRT frozen graph")
ps.add_argument("--labels", type=str, help="Path to labels file")
ps.add_argument("--microphone", type=str, help="IPv4 and port of LOUROE Microphone TCP socket")
ps.add_argument("--email", type=str, help="Valid gmail address")
ps.add_argument("--password", type=str, help="Valid gmail password for email")
ps.add_argument("--recipients", type=str, help="List of recipients for gun detection image",
        default=['CIMCONGunDetector@gmail.com'])
ps.add_argument("--video", type=str, help="Path to video feed")

opt, argv = ps.parse_known_args()

# Number of objects the neural network can classify. For now, it can only
# classify AK-47s or any type of gun that resembles it.
NUM_CLASSES = 1


# Function to retrieve frozen graph file
def get_frozen_graph(graph_file):
    """Read Frozen Graph file from disk.

    Parameters
    ----------
    graph_file : string
        Path to frozen graph file

    Returns
    -------
    graph_def : tf.GraphDef
        Graph definition in shared memory
    """
    with tf.gfile.FastGFile(graph_file, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
    return graph_def


# Retrieves the actual frozen graph
detection_graph = get_frozen_graph(opt.model)

# Config for TensorRT
tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True       # Most useful feature of TensorRT. Allows for conversion between CPU and GPU memory
sess = tf.Session(config=tf_config)
tf.import_graph_def(detection_graph, name='')


# Labels for the bounding boxes (only AK-47 so far...)
label_map = label_map_util.load_labelmap(opt.labels)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# Definite input and output Tensors for detection_graph
image_tensor = sess.graph.get_tensor_by_name('image_tensor:0')
detection_boxes = sess.graph.get_tensor_by_name('detection_boxes:0')
detection_scores = sess.graph.get_tensor_by_name('detection_scores:0')
detection_classes = sess.graph.get_tensor_by_name('detection_classes:0')
num_detections = sess.graph.get_tensor_by_name('num_detections:0')
print("Found all output layers")

# parsing of the IPv4 address of the microphone socket
ip = opt.microphone.split(':')[0]
port = int(opt.microphone.split(':')[1])

print("Assigning microphone to IP {} at port {}".format(ip, port))

# Actually binds socket to microphone output location, and
# returns True if reads b'Gunshot' in microphone output
if mic.main(ip, port):

    print("Heard a gunshot! Starting video analytics...")


    count = 0                                   # Keeps track of frame count
    vidcap = cv2.VideoCapture(opt.video)        # Uses OpenCV to parse video

    w = int(vidcap.get(3))
    h = int(vidcap.get(4))

    # initializes lists for quantifying processing speed
    read_times, proc_times, write_times, fps = [], [], [], []

    while vidcap.isOpened():
        ts = time.time()

        # Uses OpenCV to read image
        ret, img = vidcap.read()
        ts1 = time.time()

        # Stops reading once the video ends
        if img is None or ret is False:
            break

        # Converts video from OpenCV format to RGB
        image_np = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)

        # Runs the detection neural network
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})

        # Draws boxes around any guns found
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=8,
            min_score_thresh=0.25 )

        # Converts colors back into OpenCV format
        img = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)

        ts2 = time.time()

        # Detects a gun if the confidence threshhold exceeds 25%, and quits
        if scores[0][0] > 0.25:
            cv2.imwrite("gun.png".format(count), img)
            break

        ts3 = time.time()
        count += 1

        k = cv2.waitKey(10)
        if k == 27:
            break

        read_times.append(ts1 - ts)
        proc_times.append(ts2 - ts1)
        write_times.append(ts3 - ts2)
        fps.append(1 / (ts3 - ts))
        print("Read Time: {}, Processing Time: {}, Write Time: {}, FPS: {}".format(read_times[-1], proc_times[-1], write_times[-1], fps[-1]))

    vidcap.release()

    # function to aggregate processing times
    def agg(l):
        '''Aggregate time differential data

        Parameters
        ----------
        l : float[]
            List of time differentials

        Returns
        -------
        agg : float
            Median time differential
        '''
        l.sort()
        return l[int (len(l)/2)]

    print("\nAverage Read Time: {}\nAverage Processing Time: {}\nAverage Write Time: {}\nAverage FPS: {}".format(agg(read_times), agg(proc_times), agg(write_times), agg(fps)))

    # Emails the image
    email.main(opt.email, opt.password, recipients=[rec.replace(' ', '') for rec in opt.recipients.split(',')],
            image='./gun.png', text="Found a gun!")

    print("Emailed gun frame. Exiting...")

