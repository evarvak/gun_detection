#!/usr/bin/python3

'''Email script for sending images

Script is meant to be imported into detect_guns.py. It uses email.mime and smtplib to email
images. The script can be called from a terminal or the main() function can be imported
into another python script.

Command line arguments:
    --email                 A valid gmail address
    --password              A valid password corresponding to the email parameter
    --recipients            A list of recipients' email addresses
    --image                 A path to the image file to be sent (default is './gun.png')
    --text                  The text to be emailed to the recipients
    --debug                 Start in debug mode (default is False)
'''

import sys
import smtplib

from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

import argparse


def main(email, password, recipients, image='./gun.png', text=None, debug=False):

    """Sends email image from user to recipients.

    Parameters
    ----------
    email : string
        A valid gmail address
    password : string
        A valid password corresponding to the email parameter
    recipients : string[]
        A list of recipients' email addresses
    image : string, optional
        A path to the image file to be sent (default is './gun.png')
    text : string, optional
        The text to be emailed to the recipients (default is \'\')
    debug : bool, optional
        Start in debug mode (default is False)

    Returns
    -------
    None

    """

    # address = Address("Edward Varvak", 'evarvak', 'gmail.com')
    # recipients = ['edward.varvak@cimconlighting.com', 'evarvak@gmail.com']

    print("Sending email to recipients {}".format(recipients))

    msg = MIMEMultipart()
    msg['Subject'] = text
    msg['From'] = email
    msg['To'] = ', '.join(recipients)

    with open(image, 'rb') as img:

        image = MIMEImage(img.read())

    msg.attach(image)

    server = smtplib.SMTP(host='smtp.gmail.com', port=587)
    server.set_debuglevel(debug)
    server.starttls()
    server.ehlo()
    server.login(email, password)
    server.send_message(msg)
    server.quit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Sends an image from the user's email to a list of recievers",
            formatter_class=argparse.RawTextHelpFormatter,
            )
    parser.add_argument("--email", type=str, help="Email address of the sender")
    parser.add_argument("--password", type=str, help="Password of sender's email")
    parser.add_argument("--recipients", type=str, help="Email addresses of recipients")
    parser.add_argument("--image", type=str, default='./gun.png',
            help="Path to the image to be emailed")
    parser.add_argument("--text", type=str, default='', help='Text to be emailed')
    parser.add_argument("--debug", type=bool, default=False, help='Debug mode (default is False)')

    opt, argv = parser.parse_known_args()

    main(email=opt.email, password=opt.password, recipients=opt.recipients.split(', '), image=opt.image, text=opt.text, debug=opt.debug)


