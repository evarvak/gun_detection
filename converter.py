#!/bin/bash

'''Convertion script from Tensorflow Frozen Graph to TensorRT Frozen Graph
Turns frozen tensorflow graph (frozen_inference_graph.pb) into frozen TRT graph

Run with 'python3 converter.py path/to/frozen_inference_graph.pb'
Outputs 'trt_graph.pb' in frozen graph's directory
'''


#TODO Inplement exceptions into functions

import tensorflow as tf

import tensorflow.contrib.tensorrt as trt
from tensorflow.python.framework import graph_io

import sys


def get_frozen_graph(graph_file):
    """Read Frozen Graph file from disk.

    Parameters
    ----------
    graph_file : string
        Path to frozen graph file

    Returns
    -------
    graph_def : tf.GraphDef
        Tensorflow structure for graph definition
    """
    with tf.gfile.FastGFile(graph_file, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
    return graph_def


def make_tensorrt_graph(graph_def, out_loc):

    '''Makes TensorRT graph out of tensorflow GraphDef

    Parameters
    ----------
    graph_def : tf.GraphDef
        Input tensorflow graph to be converted into TensorRT
    out_loc : string
        Output location WITHOUT output file name
        Will be saved as {out_loc}/trt_graph.pb

    Returns
    -------
    output_path : string
        Path to the output graph
    '''

    trt_graph = trt.create_inference_graph(
        input_graph_def=graph_def,
        outputs=['detection_boxes', 'detection_scores', 'detection_classes', 'num_detections'],
        max_batch_size=1,
        max_workspace_size_bytes=1 << 25,
        precision_mode='FP16',
        minimum_segment_size=50
    )

    output_path = graph_io.write_graph(trt_graph, out_loc, "trt_graph.pb", as_text=False)
    return output_path


def main(input_file):

    '''Fully converts tensorflow frozen graph into TensorRT frozen graph

    Parameters
    ----------
    input_file : string
        Path to tensorflow frozen graph (frozen_inference_graph.pb)

    Returns
    -------
    value : int, unsigned
        0 if no exceptions else 1
    '''

    path = '/'.join(input_file.split('/')[:-1])

    print("Unfreezing tensorflow graph at {}".format(input_file))
    graph = get_frozen_graph(input_file)

    print("Retrieved graph definition. Converting to TensorRT...")
    graph = make_tensorrt_graph(graph, path)

    print("Saved TensorRT inference graph to {}. Exiting...".format(graph))
    return 0

if __name__ == '__main__':
    graph_loc = sys.argv[1]

    code = main(graph_loc)
    exit(code)


